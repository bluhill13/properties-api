﻿using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.Agent
{
    public class AgentPropertyDetail
    {
        //[Required]

        public int Id { get; set; }
        //[Required]
        //[MaxLength(255)]
        public string Name { get; set; }

        public string Line { get; set; }

        public string Line2 { get; set; }

        public string Municipality { get; set; }

        public string City { get; set; }

        public int Zip { get; set; }

        public DateTime CreatedAt { get; set; }


        public PropertyType PropertyType { get; set; }

        public PropertyTypeEnum PropertyTypeId { get; set; }
        public PropertyStatus PropertyStatus { get; set; }
        public PropertyStatusEnum PropertyStatusId { get; set; }


        public ICollection<string> OwnershipLogs { get; set; }

        public ICollection<String> Renovations { get; set; }

        public ICollection<string> Valuations { get; set; }

  
        
        public string[] PerviousOwners { get; set; }

        public List<string> PropertyImages{ get; set; }

        public string AccountType { get; set; } = Enum.GetName(typeof(AccountTypeEnum), AccountTypeEnum.Agent);

    }
}
