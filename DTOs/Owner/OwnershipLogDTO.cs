﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.Owner
{
    public class OwnershipLogDTO
    {
        public int OwnerId { get; set; }

        public int PropertyId { get; set; }

        public DateTime DateAquired { get; set; }

        public DateTime DateSold { get; set; }
    }
}
