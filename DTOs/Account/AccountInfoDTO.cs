﻿using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.Account
{
    public class AccountInfoDTO
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; } = "not defined";
        public string username { get; set; } = "not defined";
        [Required]
        [MaxLength(255)]
        public string Surname { get; set; } = "not defined";

        public bool Active { get; set; } = true;
        public string Email { get; set; } = "no email registered";

        public string Role { get; set; } = "Not defined";
        
    }
}
