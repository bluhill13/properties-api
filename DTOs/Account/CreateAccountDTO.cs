﻿using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.Account
{

    public class CreateAccountDTO
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; } = "test";

        [Required]
        [MaxLength(255)]
        public string Surname { get; set; } = "testerson";

        public bool Active { get; set; } = true;
        public string Email { get; set; } = "";

        public DateTime CreatedAt { get; set; } = DateTime.Now;

        public AccountTypeEnum AccountTypeId {get;set;} = AccountTypeEnum.Buyer;
    }
}
