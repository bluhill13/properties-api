﻿using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.PropertyDTO
{
    public class PropertyDTO
    {
        public int Id { get; set; }

        public string Line { get; set; } 

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }
        


        public string PropertyStatus { get; set; }

        public string PropertyType { get; set; }

        public List<string> PropertyImages { get; set; }

        public string AccountType { get; set; } = Enum.GetName(typeof(AccountTypeEnum), AccountTypeEnum.Guest);

    }
}
