﻿using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.DTOs.Buyer
{
    public class BuyerPropertyDetail
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Line { get; set; }

        public string Line2 { get; set; }

        public string Municipality { get; set; }

        public string City { get; set; }

        public int Zip { get; set; }

        public DateTime CreatedAt { get; set; }

        public List<String> Renovations { get; set; }
        public List<string> Valuations { get; set; }

        public string PropertyStatus { get; set; }

        public string PropertyType { get; set; }

        public List<string> PropertyImages { get; set; }

        public string AccountType { get; set; } = Enum.GetName(typeof(AccountTypeEnum), AccountTypeEnum.Buyer);

    }
}
