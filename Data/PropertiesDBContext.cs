﻿using Microsoft.EntityFrameworkCore;
using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Data
{
    public class PropertiesDBContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }

        public DbSet<Property> Properties { get; set; }

        public DbSet<PropertyImage> PropertyImages { get; set; }

        public DbSet<PropertyStatus> PropertyStatuses { get; set; }

        public DbSet<PropertyType> PropertyTypes { get; set; }

        public DbSet<Renovation> Renovations { get; set; }

        public DbSet<Valuation> Valuations { get; set; }

        public DbSet<Owner> Owners { get; set; }

        public DbSet<OwnerType> OwnerTypes { get; set; }

        public DbSet<OwnershipLog> OwnershipLogs { get; set; }

        public PropertiesDBContext(DbContextOptions<PropertiesDBContext> options) : base(options)
        {

        }

        public PropertiesDBContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
    }
}
