﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Data
{
    /// <summary>
    /// This class seeds data into the database at context build.
    /// </summary>
    public static class ModelBuilderExtensions
    {
        static string pictureSource =
            "https://images.pexels.com/photos/186077/pexels-photo-186077.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

        static string livingRoomSrc =
            "https://images.pexels.com/photos/4078617/pexels-photo-4078617.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

        static string bathroomSrc =
            "https://images.pexels.com/photos/342800/pexels-photo-342800.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

        static string kitchenSrc =
            "https://images.pexels.com/photos/2724748/pexels-photo-2724748.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

        static List<string> pictureSources = new List<string>() {
            "https://images.unsplash.com/photo-1573544209644-4e154f09671c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=658&q=80",
            "https://images.unsplash.com/photo-1512917774080-9991f1c4c750?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
            "https://images.unsplash.com/photo-1570544820236-d542f7512ce4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1575&q=80",
            "https://images.unsplash.com/photo-1449844908441-8829872d2607?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
            "https://images.unsplash.com/photo-1564631027824-92f5b553a7e2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
            "https://images.unsplash.com/photo-1602941525346-b707b1cbe7fe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1342&q=80",
            "https://images.unsplash.com/photo-1597047084897-51e81819a499?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1349&q=80",
            "https://images.unsplash.com/photo-1564631028405-d1df1437c19c?ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
            "https://images.unsplash.com/photo-1553524805-28dbfc2f84fd?ixlib=rb-1.2.1&auto=format&fit=crop&w=1491&q=80"
    };

        public static void Seed(this ModelBuilder modelBuilder)
        {
            //Seeding the two AccountTypes
            foreach (AccountTypeEnum accountType in (AccountTypeEnum[])Enum.GetValues(typeof(AccountTypeEnum)))
            {
                modelBuilder.Entity<AccountType>().HasData(new AccountType
                {
                    Id = accountType,
                    Name = Enum.GetName(typeof(AccountTypeEnum), accountType)
                });
            }

            //Seeding the four PropertyTypes
            foreach (PropertyTypeEnum propertyType in (PropertyTypeEnum[])Enum.GetValues(typeof(PropertyTypeEnum)))
            {
                modelBuilder.Entity<PropertyType>().HasData(new PropertyType
                {
                    Id = propertyType,
                    Name = Enum.GetName(typeof(PropertyTypeEnum), propertyType)
                });
            }


            //Seeding the OwnerType
            foreach (OwnerTypeEnum ownerType in (OwnerTypeEnum[])Enum.GetValues(typeof(OwnerTypeEnum)))
            {
                modelBuilder.Entity<OwnerType>().HasData(new OwnerType
                {
                    Id = ownerType,
                    Name = Enum.GetName(typeof(OwnerTypeEnum), ownerType)
                });
            }

            

            //Seeding the PropertyStatus
            foreach (PropertyStatusEnum propertyStatus in (PropertyStatusEnum[])Enum.GetValues(typeof(PropertyStatusEnum)))
            {
                modelBuilder.Entity<PropertyStatus>().HasData(new PropertyStatus
                {
                    Id = propertyStatus,
                    Name = Enum.GetName(typeof(PropertyStatusEnum), propertyStatus)
                });
            }


            //First property has special things for demo purposes

            //Seeding Owner
            modelBuilder.Entity<Owner>().HasData(new Owner()
            {
                Id = 1,
                Name = "Knut",
                Surname = "Magnusen",
                CreatedAt = DateTime.Now,
                DateOfBirth = DateTime.MinValue,
                DNumber = 1234,
                Email = "Ownerson@property.com",
                PhoneNumber = 123456789,

            });


            //Seeding property
            modelBuilder.Entity<Property>().HasData(new Property()
            {
                Id = 1,
                Name = "Beautiful, white-fenced sub urban house",
                City = "Bergen",

                CreatedAt = DateTime.MinValue,

                Line = "Paradisgaten",
                Line2 = "9",

                Municipality = "Bergen Kommune",

                PropertyStatusId = PropertyStatusEnum.AVAILABLE,
                PropertyTypeId = PropertyTypeEnum.HOUSE,
                OwnerTypeId = OwnerTypeEnum.PRIVATE,
                Zip = 5000,

            });

            //Seeding pictures
            modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
            {
                Id = 1,
                PropertyId = 1,
                Url = pictureSource,
            });

            modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
            {
                Id = 1000 + 1,
                PropertyId = 1,
                Url = livingRoomSrc,
            });
            modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
            {
                Id = 2000 + 1,
                PropertyId = 1,
                Url = kitchenSrc,
            });
            modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
            {
                Id = 3000 + 1,
                PropertyId = 1,
                Url = bathroomSrc,
            });
            //Ownershiplogs:
            modelBuilder.Entity<OwnershipLog>().HasData(new OwnershipLog
            {
                Id = 1,
                PropertyId = 1,
                OwnerId = 1,
                CreatedAt = DateTime.Now
            });

            //Valuations
            modelBuilder.Entity<Valuation>().HasData(new Valuation
            {

                Id = 1,
                PropertyId = 1,
                Value = 10000 + (50 - 1) * 10000,
                ValuationDate = DateTime.MinValue
            });


            modelBuilder.Entity<Valuation>().HasData(new Valuation
            {

                Id = -1,
                PropertyId = 1,
                Value = 100000 + (50 - 1) * 10000,
                ValuationDate = DateTime.Now
            });


            //Renovations
            modelBuilder.Entity<Renovation>().HasData(new Renovation
            {

                Id = 1,
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now,
                Description = "Electrical was renewed for EU code.",
                PropertyId = 1
            });

            modelBuilder.Entity<Renovation>().HasData(new Renovation
            {

                Id = -1,
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now,
                Description = "Kitchen was remodeled with new sink.",
                PropertyId = 1
            });






            for (int i = 2; i < 101; i++)
            {
               Property property =  new Property()
                {
                    Id = i,
                    Name = "Adjective, adjective Building-Type in Location",
                    City = "Bergen",

                    CreatedAt = DateTime.Now,

                    Line = "Lorem ipsum",
                    Line2 = "8A",

                    Municipality = "Bergen Kommune",

                    PropertyStatusId = PropertyStatusEnum.AVAILABLE,
                    PropertyTypeId = PropertyTypeEnum.HOUSE,
                    OwnerTypeId = OwnerTypeEnum.PRIVATE,
                    Zip = 5000,

                };

                Owner owner = new Owner()
                {
                    Id = i,
                    Name = "Owner",
                    Surname = "Ownerson",
                    CreatedAt = DateTime.Now,
                    DateOfBirth = DateTime.Now,
                    DNumber = 1234,
                    Email = "Ownerson@property.com",
                    PhoneNumber = 123456789,

                };

                //Seeding Owner
                modelBuilder.Entity<Owner>().HasData(owner);


                //Seeding properties
                modelBuilder.Entity<Property>().HasData(property);

                //Seeding pictures
                modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
                {
                    Id = i,
                    PropertyId = i,
                    Url = pictureSources[i% (pictureSources.Count)],
                });

                modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
                {
                    Id = 1000+i,
                    PropertyId = i,
                    Url = livingRoomSrc,
                });
                modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
                {
                    Id = 2000+i,
                    PropertyId = i,
                    Url = kitchenSrc,
                });
                modelBuilder.Entity<PropertyImage>().HasData(new PropertyImage
                {
                    Id = 3000+i,
                    PropertyId = i,
                    Url = bathroomSrc,
                });
                //Ownershiplogs:
                modelBuilder.Entity<OwnershipLog>().HasData(new OwnershipLog
                {
                    Id = i,
                    PropertyId = i,
                    OwnerId = i,
                    CreatedAt = DateTime.Now
                });

                //Valuations
                modelBuilder.Entity<Valuation>().HasData(new Valuation
                {

                    Id = i,
                    PropertyId = i,
                    Value = 10000 + (50-i)*10000,
                    ValuationDate = DateTime.Now
                }) ;

                //Renovations
                modelBuilder.Entity<Renovation>().HasData(new Renovation
                {

                    Id = i,
                    DateFrom = DateTime.Now,
                    DateTo = DateTime.Now,
                    Description = "Kitchen was remodeled for a 2010 standard.",
                    PropertyId = i
                });

            }
        }
    }
    
}
