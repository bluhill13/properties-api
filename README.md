# properties-api

![banner](https://i.pinimg.com/originals/27/02/48/2702489cfb0b9f8aec2f827ae461f528.jpg)

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

properties-API provides an api for database handling for real estate companies.

properties-API is a project by Bendik Sperrevik, Carl Søilen-Knutsen and Thomas Blaalid. The API's main function is to provide a RESTful api experience for calls to database. By the time you read this README, the Cloud service part of the API will have beeen deprecated (due to costs), but by creating your own local instance of an sql database, it should be possible to get it up and running.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background
This is part of Experis Academy Bergen's graduating case. This part is only the front end
## Install

`dotnet restore`

## Usage

The application can be started in development mode using the following command:

`dotnet run`

In powershell, these need to be set in user-secrets:

`TokenSecrets:Auth = https://propertiesauthentication.azurewebsites.net/auth/realms/propertiesAuth`

`TokenSecrets:Audience = account`



And a connection string with your sql-database instance which should be called "PropertyDB"


Lastly, run the migrations:

`dotnet ef database update`

## API
Please read the [api documentation](ApiDocumentation.pdf)

## Maintainers

[@bluhill13](https://github.com/bluhill13)

## Contributing



Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Thomas Blaalid
