﻿using AutoMapper;
using propertiesAPI.DTOs.Agent;
using propertiesAPI.DTOs.Buyer;
using propertiesAPI.DTOs.PropertyDTO;
using propertiesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Profiles
{
    public class PropertyProfile : Profile
    {
        public PropertyProfile()
        {

            var creationMap = CreateMap<Property, PropertyDTO>();
            creationMap.ReverseMap();


            var agentMap = CreateMap<Property, AgentPropertyDetail>();
            agentMap.ForMember(dest => dest.OwnershipLogs, src => src.MapFrom(property => property.OwnershipLogs.Select(ol => ol.Owner)));
            agentMap.ForMember(dest => dest.Renovations, src => src.MapFrom(property => property.Renovations.Select(r => r.Description).ToList()));
            agentMap.ForMember(dest => dest.Valuations, src => src.MapFrom(property => property.Valuations.Select(v => v.Value).ToList()));
            agentMap.ForMember(dest => dest.PropertyImages, opt => opt.MapFrom(
                src =>
                    src.PropertyImages.Select(img => img.Url).ToArray()
                ));

            var renovationMap = CreateMap<Renovation, AgentPropertyDetail>();
            renovationMap.ReverseMap();

            var buyerMap = CreateMap<Property, BuyerPropertyDetail>();

            buyerMap.ForMember(dest => dest.Renovations, src => src.MapFrom(property => property.Renovations.Select(r => r.Description).ToList()));
            buyerMap.ForMember(dest => dest.Valuations, src => src.MapFrom(property => new List<string>() {property.Valuations.FirstOrDefault().Value.ToString()}));
            buyerMap.ForMember(dest => dest.PropertyImages, opt => opt.MapFrom(
                src =>
                    src.PropertyImages.Select(img => img.Url).ToArray()
                )
            );
            buyerMap.ReverseMap();


            var ownerMap = CreateMap<Owner, AgentPropertyDetail>();
            ownerMap.ReverseMap();
            

            var readMap = CreateMap<Property, PropertyDTO>();
            readMap.ForMember(dest => dest.PropertyImages, opt => opt.MapFrom(
               src =>
               new List<string>() { src.PropertyImages.FirstOrDefault().Url}
               )
            ); ;
            readMap.ReverseMap();

        }
    }
}