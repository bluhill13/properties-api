﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using propertiesAPI.DTOs.Account;
using propertiesAPI.Models;

namespace propertiesAPI.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<CreateAccountDTO, Account>();
            CreateMap<Account, AccountInfoDTO>();
            
            
        }
    }
}
