using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using propertiesAPI.Data;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

using propertiesAPI.Services;
using Microsoft.AspNetCore.Http;
using propertiesAPI.Controllers;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;
using System;
using Swashbuckle.AspNetCore.Filters;

namespace propertiesAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddSwaggerExamples();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Property API", Version = "v1" });
                c.ExampleFilters();
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);


                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>(); // Adds "(Auth)" to the summary so that you can see which endpoints have Authorization
                                                                              // or use the generic method, e.g. c.OperationFilter<AppendAuthorizeToSummaryOperationFilter<MyCustomAttribute>>();
                                                                              // add Security information to each operation for OAuth2
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                // or use the generic method, e.g. c.OperationFilter<SecurityRequirementsOperationFilter<MyCustomAttribute>>();
                // if you're using the SecurityRequirementsOperationFilter, you also need to tell Swashbuckle you're using OAuth2
                c.AddSecurityDefinition("openid-connect", new OpenApiSecurityScheme
                {
                    Description = "Standard openid-connect Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.OpenIdConnect
                });


            });


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Authority = Configuration["TokenSecrets:Auth"];
                options.Audience = Configuration["TokenSecrets:Audience"];
            });

            services.AddDbContext<PropertiesDBContext>(
            option => option.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddControllers();
            //MvcOptions.EnableEndpointRouting = false;
            services.AddAutoMapper(typeof(Startup));
           
                services.AddDbContext<PropertiesDBContext>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                                         b => b.MigrationsAssembly(typeof(PropertiesDBContext).Assembly.FullName)));
                services.AddHttpContextAccessor();
                services.AddSingleton<IUriService>(o =>
                {
                    var accessor = o.GetRequiredService<IHttpContextAccessor>();
                    var request = accessor.HttpContext.Request;
                    var uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                    return new UriService(uri);
                });
                services.AddAutoMapper(typeof(Startup));
                services.AddControllers();
                
            services.AddAutoMapper(typeof(Startup));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }



            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(options => options.WithOrigins(Configuration["AllowedHosts"]).AllowAnyMethod().AllowAnyHeader());

            app.UseSwagger();


            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
