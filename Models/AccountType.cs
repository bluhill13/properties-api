﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public enum AccountTypeEnum: int
    {
        Guest,
        Buyer,
        Agent
    }

    public class AccountType
    {
        [Required]
        public AccountTypeEnum Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public ICollection<Account> Accounts { get; set; }
    }
}
