using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class Owner
    {
        [Required]

        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        [Required]
        [MaxLength(255)]
        public string Surname { get; set; }

        public int PhoneNumber { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }

        public DateTime DateOfBirth { get; set; }

        public int DNumber { get; set; }

        public DateTime CreatedAt { get; set; }

        public ICollection<OwnershipLog> OwnershipLogs{ get; set; }

    }
}
