﻿using System;
using System.ComponentModel.DataAnnotations;

namespace propertiesAPI.Models
{

    public class Account
    {
        [Key]
        public string Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        [Required]
        [MaxLength(255)]
        public string Surname { get; set; }
        [MaxLength(255)]
        public int Phone { get; set; }
        [MaxLength(255)]
        public string Email { get; set; }
        [MaxLength(255)]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public bool Active { get; set; }
        [MaxLength(255)]
        public DateTime CreatedAt { get; set; }


        public AccountType AccountType { get; set; }

        public AccountTypeEnum AccountTypeId { get; set; }

    }
}
