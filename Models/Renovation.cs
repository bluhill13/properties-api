﻿using propertiesAPI.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class Renovation
    {
        [Required]
        public int Id { get; set; }

        public string Description { get; set; }

        public DateTime DateFrom { get; set; }

        public DateTime DateTo { get; set; }

        public Property Property { get; set; }
        public int PropertyId { get; set; }
    }
}
