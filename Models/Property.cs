﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class Property
    {
        
        [Required]

        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public string Line { get; set; }

        public string Line2 { get; set; }

        public string Municipality { get; set; }

        public string City { get; set; }

        public int Zip { get; set; }

        public DateTime CreatedAt { get; set; }


        public PropertyType PropertyType { get; set; }

        public PropertyTypeEnum PropertyTypeId { get; set; }
        public PropertyStatus PropertyStatus { get; set; }
        public PropertyStatusEnum PropertyStatusId { get; set; }

        public OwnerTypeEnum OwnerTypeId { get;  set; }

        public OwnerType OwnerType { get; set; }

        public ICollection<OwnershipLog> OwnershipLogs { get; set; }
        public int OwnershipLogId { get; set; }

        public ICollection<Renovation> Renovations { get; set; }

        public ICollection<Valuation> Valuations { get; set; }

        public ICollection<PropertyImage> PropertyImages { get; set; }

    }
}
