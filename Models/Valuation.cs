﻿using propertiesAPI.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class Valuation
    {
        [Required]

        public int Id { get; set; }

        public string Comments { get; set; }

        public int Value { get; set; }
        
        public DateTime ValuationDate { get; set; }

        public int PropertyId { get; set; }
        public Property Property { get; set; }
    }
}
