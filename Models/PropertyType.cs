﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public enum PropertyTypeEnum
    {
        COMMERCIAL,
        APARTMENT,
        HOUSE,
        SEMI_DETACHED
    }
    public class PropertyType
    {
        

        [Required]
        public PropertyTypeEnum Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public ICollection<Property> Properties { get; set; }
    }
   
}
