﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public enum PropertyStatusEnum
    {
        ON_MARKET,
        AVAILABLE,
        UNDER_RENOVATION,
        UNAVAILABLE,
        HISTORICAL
    }
    public class PropertyStatus
    {
        [Required]
        
        public PropertyStatusEnum Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public ICollection<Property> Properties { get; set; }

    }
}
