﻿using propertiesAPI.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class OwnershipLog
    {
        [Required]
        public int Id { get; set; }


        public DateTime DateAquired { get; set; }

        public DateTime DateSold { get; set; }

        public DateTime CreatedAt { get; set; }

        public Property Property { get; set; }
        public int PropertyId { get; set; }

        public Owner Owner { get; set; }
        public int OwnerId { get; set; }
    }
}
