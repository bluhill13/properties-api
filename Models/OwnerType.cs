﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public enum OwnerTypeEnum { 
    COMPANY,
    PRIVATE
    }
    public class OwnerType
    {
            
        [Required]
        public OwnerTypeEnum Id { get; set; }
        [Required]
        [MaxLength(255)]
        public String Name { get; set; }

        public ICollection<Property> Properties { get; set; }
    }
}
