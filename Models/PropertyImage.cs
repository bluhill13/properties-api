﻿using propertiesAPI.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace propertiesAPI.Models
{
    public class PropertyImage
    {

        [Required]
        public int Id { get; set; }

        public string Url { get; set; }

        public Property Property { get; set; }
        public int PropertyId { get; internal set; }
    }
}
