﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using propertiesAPI.Data;
using propertiesAPI.DTOs.Owner;
using propertiesAPI.Models;

namespace propertiesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OwnerController : ControllerBase
    {

        private readonly PropertiesDBContext _context;

        private readonly IMapper _mapper;

        public OwnerController(PropertiesDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all owners
        /// </summary>
        /// <returns>owner objects</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Owner>> GetAllOwners() {
            return _context.Owners;
        }
        /// <summary>
        /// Returns an owner based on id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Owner</returns>
        [HttpGet("{id}")]
        public ActionResult<Owner> GetOwnerById(int id)
        {
            var owner = _context.Owners.Find(id);
            return owner;
        }
        /// <summary>
        /// Adds a new owner
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<Owner> AddOwner(Owner owner)
        {

            _context.Owners.Add(owner);
            _context.SaveChanges();
            return owner;
        }
    }
}
