﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using propertiesAPI.Data;
using propertiesAPI.DTOs.Agent;
using propertiesAPI.DTOs.Buyer;
using propertiesAPI.DTOs.PropertyDTO;
using propertiesAPI.Filter;
using propertiesAPI.Helpers;
using propertiesAPI.Models;
using propertiesAPI.Services;
using propertiesAPI.wrappers;

namespace propertiesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PropertyController : ControllerBase
    {
        
        private readonly PropertiesDBContext _context;
        private readonly IMapper _mapper;
        private readonly IUriService uriService;
        public PropertyController(PropertiesDBContext context, IMapper mapper, IUriService uriService)
        {
            _context = context;
            _mapper = mapper;
            this.uriService = uriService;

        }

        /// <summary>
        /// Gets property by id
        /// </summary>
        /// <remarks>
        /// Returns different property object based on what role the user has.
        /// 
        /// </remarks>
        /// <param name="id"></param>
       /// <returns>A property</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBydId(int id)
        {
        


            Property property = await _context.Properties.Where(p => p.Id == id).
                Include(p => p.PropertyImages).
                Include(p => p.OwnershipLogs).
                Include(p => p.Renovations).
                Include(p => p.Valuations).
                FirstOrDefaultAsync();


            if (User.IsInRole("agent"))
            {
                return Ok(new Response<AgentPropertyDetail>(_mapper.Map<AgentPropertyDetail>(property)));
            }
            else if(User.IsInRole("buyer"))
            {
                return Ok(new Response<BuyerPropertyDetail>(_mapper.Map<BuyerPropertyDetail>(property)));
            }

            return Ok(new Response<PropertyDTO>(_mapper.Map<PropertyDTO>(property)));
        }

        
        /// <summary>
        /// Gets paginated lazy-loaded propertyDTO objects
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        
        public async Task<IActionResult> GetAll([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            var response = await _context.Properties.Include(p => p.PropertyImages)
                .Skip((validFilter.PageNumber-1)*validFilter.PageSize)
                .Take(validFilter.PageSize)
                .ToListAsync();
            var totalRecords = await _context.Properties.CountAsync();
            var pagedResponse = PaginationHelper.CreatePagedReponse<PropertyDTO>(_mapper.Map<List<PropertyDTO>>(response), validFilter, totalRecords, uriService, route);
            return Ok(pagedResponse);
        }

        }
       
}
    


