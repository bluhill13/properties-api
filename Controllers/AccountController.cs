﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using propertiesAPI.Data;
using propertiesAPI.DTOs.Account;
using propertiesAPI.Models;
using System.Security.Claims;
using System.Threading.Tasks;


namespace propertiesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly PropertiesDBContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<AccountController> _logger;
        public AccountController(PropertiesDBContext context, IMapper mapper, ILogger<AccountController> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        /// <summary>
        /// Finds Account from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Account object</returns>
        [Authorize]
        [HttpGet]

        public async Task<ActionResult<AccountInfoDTO>> GetAccountByID() 
        {
            try
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
                var account = await _context.Accounts.FindAsync(userId);
                if (account == null)
                {
                    return BadRequest();
                }
                AccountInfoDTO accountInfoDTO = _mapper.Map<AccountInfoDTO>(account);
                accountInfoDTO.username = User.FindFirst("preferred_username").Value;
                if (User.IsInRole("agent")) accountInfoDTO.Role = "Real-estate agent";
                return accountInfoDTO;
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                return NotFound();
            }
        }



        /// <summary>
        /// Creates an account in the database
        /// </summary>
        /// <param name=><param>
        /// <returns>Status: 204</returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateAccount() 
        {
            try
            {
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
                var existingAccount = await _context.Accounts.FindAsync(userId);
                if (existingAccount != null)
                {
                    return NoContent();
                }
                else
                {
                    CreateAccountDTO newAccount = new CreateAccountDTO();
                    string temp = User.FindFirst(ClaimTypes.NameIdentifier).Value.ToString();
                    newAccount.Id = temp;
                    newAccount.Active = true;
                    newAccount.CreatedAt = DateTime.Now;
                    newAccount.Name = User.FindFirst(ClaimTypes.GivenName).Value.ToString();
                    newAccount.Surname = User.FindFirst(ClaimTypes.Surname).Value.ToString();
                    newAccount.Email = User.FindFirst(ClaimTypes.Email).Value;
                    if (User.IsInRole("buyer")) newAccount.AccountTypeId = AccountTypeEnum.Buyer;
                    else if (User.IsInRole("agent")) newAccount.AccountTypeId = AccountTypeEnum.Agent;
                    Account account = _mapper.Map<Account>(newAccount);
                    await _context.Accounts.AddAsync(account);
                    _context.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return NoContent();
            }

            
        }

        /// <summary>
        /// Updates an account
        /// </summary>
        /// <param name="id">The account id</param>
        /// <param name="updatedAccount">account that is to be updated</param>
        /// <returns>Status: 204</returns>
        [Authorize]
        [HttpPut]
        public ActionResult UpdateAccount(Account updatedAccount)
        {
            return NoContent();
        }   
    }
}
